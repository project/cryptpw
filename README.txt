This module makes it possible for Courier-MTA to authenticate against your
Drupal user database.

To do this:

1. Select a user that will own all your drupal-hosted mailspools.
   (I use "www-data" which on my system is uid=33, gid=33)
2. Make sure a central mail directory is owned by that user
   (I use /var/mail)
3. Unpack this module into your sites/all/modules directory.
4. Enable this module.
5. Visit /admin/settings/cryptpw and configure it.
6. Edit your Courier configuration as the settings page suggests.
